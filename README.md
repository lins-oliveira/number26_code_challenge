This is the JAVA code challenge for Number 26 job position.

The task was developed using Spring Boot and Spring STS plugin for Eclipse IDE.

To test the project just clone it from this repository and run it as a Spring Boot application.

All APIs were implemented and are fully functional.

I didn't have time to write all documentation, write the unit tests and fine tune the code. I had to work on my master code this weekend. I would do a better job with a little bit more time.
