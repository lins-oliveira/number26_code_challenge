package eu.number26.core.repositories;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.number26.core.business.Transaction;
import eu.number26.core.business.Type;

/**
 * This class is responsible to manage the transactions.
 * 
 * Since I cannot use SQL I'm saving the transactions in a Map.
 * 
 * The map was chosen because an Array has Integer as key 
 * that is represented in 32 bits and and Long is represented
 * by 64 bits.
 *  
 * @author rodrigo
 *
 */
public class TransactionRepository {
	
	private Map<Long, Transaction> transactions;
	private Long index;
	
	public TransactionRepository() {
		this.transactions = new Hashtable<Long, Transaction>();
		this.index = 0L;
	}
	
	/**
	 * Add a new {@link Transaction} to the transaction repository.
	 * 
	 * Create a index here is not a good idea. It would be better
	 * to use the database management here.
	 * 
	 * @param transaction
	 */
	public void saveOrUpdate(Transaction transaction) {
		// Save new transaction to the repository
		if(transaction.getId() == null) {
			transaction.setId(index);
			transactions.put(index, transaction);
			index = index + 1L;
		// Update the transaction in the repository
		} else {
			transactions.put(transaction.getId(), transaction);
		}
		
	}
	
	/**
	 * This method returns a Transaction based on its id.
	 * 
	 * @param id
	 * @return {@link Transaction}
	 */
	public Transaction getTransactionById(Long id) {
		return transactions.get(id);
	}
	
	public List<Transaction> getTransactionsByType(Type type) {
		List<Transaction> transactions = new ArrayList<Transaction>();
		Set<Long> keys = this.transactions.keySet();
		for(Long key : keys) {
			Transaction t = this.transactions.get(key); 
			if(t.getType().getName() == type.getName()) {
				transactions.add(t);
			}
		}
		return transactions;
	}
	
	public Double getTransactionSum(Transaction transaction) {
		Double sum;
		if(transaction == null){
			return 0.0;
		}
		
		Transaction t = transactions.get(transaction.getId());
		sum = t.getAmount();
		if(t.getParent() != null){
			return sum + getTransactionSum(t.getParent());
		}
		return sum;
	}

	/*public void debug() {
		Set<Long> keys = this.transactions.keySet();
		for(Long key : keys){
			System.out.println("KEY: " + key + " | Transaction: " + this.transactions.get(key));
		}
	}*/
}
