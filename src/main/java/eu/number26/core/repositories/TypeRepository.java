package eu.number26.core.repositories;

import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import eu.number26.core.business.Type;

public class TypeRepository {
	
	private Map<Long, Type> types;
	private Long index;
	
	public TypeRepository() {
		this.types = new Hashtable<Long, Type>();
		this.index = 0L;
	}
	
	public void saveOrUpdate(Type type) {
		// Save new transaction to the repository
		if(type.getId() == null) {
			Set<Long> keys = types.keySet();
			for(Long key : keys) {
				Type t = types.get(key);
				if(t.getName() == type.getName()){
					return;
				}
			}
			type.setId(index);
			types.put(index, type);
			index = index + 1L;
		// Update the transaction in the repository
		} else {
			types.put(type.getId(), type);
		}
		
	}
	
	public Type getTypeById(Long id){
		return this.types.get(id);
	}
	
	/**
	 * This method returns {@link Type} based on its name.
	 * 
	 * Would be a better idea to use the name as the key
	 * from this object. However, to keep consistency, I kept
	 * the id as the identifier.
	 * 
	 * Using a database would be much simpler to retrieve
	 * the objects.
	 * 
	 * @param name
	 * @return
	 */
	public Type getTypeByName(String name){
		Set<Long> keys = this.types.keySet();
		for(Long key : keys){
			if(this.types.get(key).getName().equals(name)){
				return this.types.get(key);
			}
		}
		return null;
	}
	
	public void debug(){
		System.out.println("Debugging TypeRepository");
		Set<Long> keys = this.types.keySet();
		for(Long key : keys){
			System.out.println(this.types.get(key));
		}
	}
}
