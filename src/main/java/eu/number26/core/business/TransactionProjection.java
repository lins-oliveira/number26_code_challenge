package eu.number26.core.business;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is a representation of a Transaction to be displayed at a JSON
 * response.
 * 
 * It works as a Adapter class. Spring boot has a projection API, but it is
 * built-in in the data persistence API.
 * 
 * @author rodrigo
 *
 */
public class TransactionProjection {

	private Double amount;
	private String type;
	private Long parent_id;
	
	public TransactionProjection(Transaction transaction) {
		this.amount = transaction.getAmount();
		this.type = transaction.getType().getName();
		if(transaction.getParent() != null) {
			this.parent_id = transaction.getParent().getId();
		}
	}

	@JsonCreator
	public TransactionProjection(@JsonProperty("amount") Double amount,
								 @JsonProperty("type") String type,
								 @JsonProperty("property_id") Long parent_id) {
		super();
		this.type = type;
		this.amount = amount;
		this.parent_id = parent_id;
	}

	public Double getAmount() {
		return amount;
	}

	public String getType() {
		return type;
	}

	public Long getParent_id() {
		return parent_id;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setParent_id(Long parent_id) {
		this.parent_id = parent_id;
	}

	@Override
	public String toString() {
		return "TransactionProjection [amount=" + amount + ", type=" + type
				+ ", parent_id=" + parent_id + "]";
	}
	
}
