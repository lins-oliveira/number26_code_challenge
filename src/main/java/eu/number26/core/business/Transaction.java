package eu.number26.core.business;

public class Transaction {
	
	private Long id;
	private Double amount;
	private Type type;
	private Transaction parent;
	
	public Transaction(Double amount, Type type) {
		super();
		this.amount = amount;
		this.type = type;
	}
		
	public Transaction(Double amount, Type type, Transaction parent) {
		super();
		this.amount = amount;
		this.type = type;
		this.parent = parent;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Double getAmount() {
		return amount;
	}
	
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
	
	public Transaction getParent() {
		return parent;
	}
	
	public void setParent(Transaction parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", amount=" + amount + ", type="
				+ type + ", parent=" + parent + "]";
	}
	
}
