package eu.number26.core.business;

public class Type {
	
	private Long id;
	private String name;
	
	public Type(String name) {
		super();
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Type [id=" + id + ", name=" + name + "]";
	}
	
}
