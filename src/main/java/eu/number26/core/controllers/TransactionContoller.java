package eu.number26.core.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.number26.core.business.Transaction;
import eu.number26.core.business.TransactionProjection;
import eu.number26.core.business.Type;
import eu.number26.core.repositories.TransactionRepository;
import eu.number26.core.repositories.TypeRepository;

@RestController
@RequestMapping("/transactionservice")
public class TransactionContoller {
	
	private TransactionRepository transactionRepository;
	private TypeRepository typeRepository;
	
	public TransactionContoller() {
		this.transactionRepository = new TransactionRepository();
		this.typeRepository = new TypeRepository();
		
		Type carType = new Type("cars");
		typeRepository.saveOrUpdate(carType);
		
		Type shoppingType = new Type("shopping");
		typeRepository.saveOrUpdate(shoppingType);
		
		carType = typeRepository.getTypeByName("cars");
		shoppingType = typeRepository.getTypeByName("shopping");
		
		Transaction carTransaction = new Transaction(new Double(399.00), carType, null);
		transactionRepository.saveOrUpdate(carTransaction);
		
		Transaction shoppingTransaction = new Transaction(new Double(499.00), shoppingType, null);
		transactionRepository.saveOrUpdate(shoppingTransaction);
	}

    @RequestMapping(value = "/transaction/{id:[\\d]+}", method = RequestMethod.GET)
    public ResponseEntity<TransactionProjection> getTrasactionById(@PathVariable final Long id) {
    	if(id == null){
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    	}
    	return ResponseEntity.ok(new TransactionProjection(transactionRepository.getTransactionById(id)));
    }
    
    @RequestMapping(value = "/types/{type}", method = RequestMethod.GET)
    public ResponseEntity<List<Long>> getTransactionsByTypeName(@PathVariable final String type) {
    	Type t = typeRepository.getTypeByName(type);
    	if(t == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    	}
    	List<Transaction> transactions = transactionRepository.getTransactionsByType(t);
    	List<Long> transactionsIds = new ArrayList<Long>();
    	if(transactions != null) {
    		for(Transaction transaction : transactions){
    			transactionsIds.add(transaction.getId());
    		}
    	}
    	return ResponseEntity.ok(transactionsIds);
    }
    
    @RequestMapping(value = "/transaction/", method = RequestMethod.POST)
    public ResponseEntity<String> saveTransaction(@RequestBody TransactionProjection projection) {
    	if(projection.getAmount() == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    	}
    	if(projection.getType() == null || "".equals(projection.getType())) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    	}
    	
    	Type type = typeRepository.getTypeByName(projection.getType());
    	if(type == null) {
    		type = new Type(projection.getType());
    	}
    	typeRepository.saveOrUpdate(type);
    	
    	Transaction transaction = new Transaction(projection.getAmount(), type);
    	
    	if(projection.getParent_id() != null) {
    		Transaction parent = transactionRepository.getTransactionById(projection.getParent_id());
    		if(parent != null) {
    			transaction.setParent(parent);
    		}
    	}

    	transactionRepository.saveOrUpdate(transaction);
    	
    	return ResponseEntity.status(HttpStatus.CREATED).body("{ \"status\": \"ok\" }");
    }
    
    @RequestMapping(value = "/transaction/{id:[\\d]+}", method = RequestMethod.PUT)
    public ResponseEntity<String> updateTransaction(@PathVariable final Long id, 
    															   @RequestBody TransactionProjection projection) {
    	Transaction transaction = transactionRepository.getTransactionById(id);
    	if(transaction == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    	}
    	if(projection.getAmount() == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    	}
    	if(projection.getType() == null || "".equals(projection.getType())) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    	}
    	if(projection.getParent_id() != null) {
    		Transaction parent = transactionRepository.getTransactionById(projection.getParent_id());
    		if(parent == null) {
    			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    		}
    		transaction.setParent(parent);
    	}
    	transaction.setAmount(projection.getAmount());
    	Type type = typeRepository.getTypeByName(projection.getType());
    	if(type == null) {
    		type = new Type(projection.getType());
    		typeRepository.saveOrUpdate(type);
    	}
    	transaction.setType(type);
    	transactionRepository.saveOrUpdate(transaction);
    	return ResponseEntity.ok("{ \"status\": \"ok\" }");
    }
    
    @RequestMapping(value = "/sum/{id:[\\d]+}", method = RequestMethod.GET)
    public ResponseEntity<String> getTrasactionSum(@PathVariable final Long id) {
    	if(id == null){
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    	}
    	
    	Transaction transaction = transactionRepository.getTransactionById(id);
    	if(transaction == null){
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    	}
    	
    	Double sum = transactionRepository.getTransactionSum(transaction);
    	
    	return ResponseEntity.ok("{\"sum\":" + sum + "}");
    }
}
